import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class FoodGUI {
    private JPanel root;
    private JButton gyukarubiButton;
    private JButton butakarubiButton;
    private JButton haramiButton;
    private JButton tanButton;
    private JButton kimuchiButton;
    private JButton beerButton;
    private JButton checkOutButton;
    private JTextArea orderList;
    private JLabel totalpriceLabel;

    int TotalPrice = 0;

    void order (String food, int price) {
        int order_confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Ooder Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(order_confirmation == 0) {
            String currentText = orderList.getText();
            orderList.setText(currentText + food + " " + price + "yen\n");
            JOptionPane.showMessageDialog(null,
                                          "Thank you for ordered "
                                                  + food + "! It will be served as soon as possible.");
            TotalPrice += price;
            totalpriceLabel.setText(TotalPrice + " yen");
        }
    }

    public FoodGUI() {
        gyukarubiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyukarubi",400);
            }
        });
        gyukarubiButton.setIcon(new ImageIcon(this.getClass().getResource("Gyukarubi.jpg")));
        gyukarubiButton.setText("");

        butakarubiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Butakarubi",350);
            }
        });
        butakarubiButton.setIcon(new ImageIcon(this.getClass().getResource("Butakarubi.jpg")));
        butakarubiButton.setText("");

        haramiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Harami",300);
            }
        });
        haramiButton.setIcon(new ImageIcon(this.getClass().getResource("Harami.jpg")));
        haramiButton.setText("");

        tanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tan",500);
            }
        });
        tanButton.setIcon(new ImageIcon(this.getClass().getResource("Tan.jpg")));
        tanButton.setText("");

        kimuchiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kimuchi",200);
            }
        });
        kimuchiButton.setIcon(new ImageIcon(this.getClass().getResource("Kimuchi.jpg")));
        kimuchiButton.setText("");

        beerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Beer",250);
            }
        });
        beerButton.setIcon(new ImageIcon(this.getClass().getResource("Beer.jpg")));
        beerButton.setText("");

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int checkout_confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(checkout_confirmation == 0) {
                    orderList.setText("");
                    String FinalAmount = totalpriceLabel.getText();
                    totalpriceLabel.setText("0 yen");
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + FinalAmount + ".");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(100,20,1050,700);
        frame.setVisible(true);
    }
}
